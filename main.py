import csv
import fixedThreshold
from dataStructures import Track, Datas_csv

with open('annex/mewo-labels.csv', newline='') as csvfile:
    dict_label = list(csv.DictReader(csvfile))
datas = Datas_csv("datas/test_X_nPKMRWK.csv")
res = fixedThreshold.fixed_threshold(datas, 0.5)
track = Track(110856)
track.assign_label(res)
track.display()

datas_res = Datas_csv("datas/test_y.dummy_KMszgeM.csv")
track2 = Track(110856)
track2.assign_label(datas_res)
track2.display()
