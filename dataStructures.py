import csv

ID = "ChallengeID"

class Label():
    def __init__(self, tag_name, dict_label):
        self.tag = tag_name
        self.category = dict_label[1][tag_name]
        self.classe = dict_label[2][tag_name]


class Track():
    def __init__(self, id):
        self.id = id
        self.labels = []

    def assign_label(self, res_dict):
        for lign in res_dict.get_dicts():
            if lign["ChallengeID"] == self.id:
                for item in lign.items():
                    if item[1] == 1:
                        self.labels.append(item[0])
                    else:
                        if item[0] in self.labels:
                            self.labels.remove(item[0])

    def display(self):
        print(f"{self.id} : ", end='')
        for label in self.labels:
            print(f"{label} ", end='')
        print()


class Datas_csv():
    def __init__(self, file=None):
        if file != None:
            with open(file, newline='') as csvfile:
                list_dict = list(csv.DictReader(csvfile))
                for lign in list_dict:
                    for item in lign.items():
                        lign[item[0]] = float(item[1])
                self.dicts = list_dict
        else:
            self.dicts = []
        self.size = len(self.dicts)

    def get_dicts(self):
        return self.dicts.copy()

    def add_line(self, line_dict):
        self.dicts.append(line_dict)
        self.size = len(self.dicts)

        def add_line(self, line_dict):
            self.dicts.append(line_dict)
            self.size = len(self.dicts)
            self.size = len(self.dicts)


class Datas_track():
    def __init__(self, res: Datas_csv):
        self.tracks = {}
        for lign in res.get_dicts():
            track = Track(lign[ID])
            track.assign_label(res)
            self.tracks[lign[ID]] = track

    def get_tracks_from_label(self, label: Label):
        res = []
        for track in self.tracks:
            if track.check_label(label):
                res.append(track)
        return res
