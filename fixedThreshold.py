from dataStructures import Datas_csv

def fixed_threshold_one_lign(lign, threshold):
    result = {}
    for item in lign.items():
        if item[0] == "ChallengeID":
            result[item[0]] = item[1]
        elif float(item[1]) > threshold:
            result[item[0]] = 1
        else:
            result[item[0]] = 0
    return result

def fixed_threshold(datas, threshold):
    result = Datas_csv()
    for index, lign in enumerate(datas.get_dicts()):
        result.add_line(fixed_threshold_one_lign(lign, threshold))
    return result
